<?php
/**
* The template for displaying the footer.
*
* @package Salient WordPress Theme
* @version 10.5
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$nectar_options = get_nectar_theme_options();
$header_format  = ( !empty($nectar_options['header_format']) ) ? $nectar_options['header_format'] : 'default';

?>
<div id="fws_603ed70c63ab7" data-column-margin="default" data-midnight="dark" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row full-width-content standard_section " style="padding-top: 0px;padding-bottom: 0px;left: 0px;margin-left: 0px;width: 1349px;visibility: visible;"><div class="row-bg-wrap" data-bg-animation="none" data-bg-overlay="false"><div class="inner-wrap"><div class="row-bg using-bg-color" style="background-color: #f0f0f0; "></div></div><div class="row-bg-overlay"></div></div><div class="col span_12 dark left">
	<div class="vc_col-sm-12 wpb_column column_container vc_column_container col no-extra-padding instance-13" data-t-w-inherits="default" data-border-radius="none" data-shadow="none" data-border-animation="" data-border-animation-delay="" data-border-width="none" data-border-style="solid" data-border-color="" data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner"><div class="column-bg-overlay-wrap" data-bg-animation="none"><div class="column-bg-overlay"></div></div>
			<div class="wpb_wrapper">
				<div id="fws_603ed70c640ed" data-midnight="" data-column-margin="default" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row inner_row  vc_row-o-content-middle standard_section    " style="padding-top: 4%; padding-bottom: .5%; "><div class="row-bg-wrap"> <div class="row-bg   " style=""></div> </div><div class="col span_12  center">
	<div class="vc_col-sm-3 vc_hidden-sm vc_hidden-xs wpb_column column_container vc_column_container col child_column no-extra-padding instance-14" data-t-w-inherits="default" data-shadow="none" data-border-radius="none" data-border-animation="" data-border-animation-delay="" data-border-width="none" data-border-style="solid" data-border-color="" data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner"><div class="column-bg-overlay-wrap" data-bg-animation="none"><div class="column-bg-overlay"></div></div>
		<div class="wpb_wrapper">
			
		</div> 
	</div>
	</div> 

	<div class="vc_col-sm-6 wpb_column column_container vc_column_container col child_column no-extra-padding instance-15" data-t-w-inherits="default" data-shadow="none" data-border-radius="none" data-border-animation="" data-border-animation-delay="" data-border-width="none" data-border-style="solid" data-border-color="" data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner"><div class="column-bg-overlay-wrap" data-bg-animation="none"><div class="column-bg-overlay"></div></div>
		<div class="wpb_wrapper">
			
<div class="wpb_text_column wpb_content_element ">
	<div class="wpb_wrapper">
		<h2 class="root-0-2-66 root-d0-0-2-90 typographyGutter-0-2-67 typographyGutterTop-0-2-68 textTransform-0-2-73 textTransform-d3-0-2-93 alignCenter-0-2-75 displayBlock-0-2-80" style="text-align: center;">STAY IN TOUCH</h2>
<p style="text-align: center;">Connect with us on email and stay in the loop with the world of Pretty Green.</p>
	</div>
</div>




		</div> 
	</div>
	</div> 

	<div class="vc_col-sm-3 wpb_column column_container vc_column_container col child_column no-extra-padding instance-16" data-t-w-inherits="default" data-shadow="none" data-border-radius="none" data-border-animation="" data-border-animation-delay="" data-border-width="none" data-border-style="solid" data-border-color="" data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner"><div class="column-bg-overlay-wrap" data-bg-animation="none"><div class="column-bg-overlay"></div></div>
		<div class="wpb_wrapper">
			
		</div> 
	</div>
	</div> 
</div></div><div id="fws_603ed70c64d43" data-midnight="" data-column-margin="default" data-bg-mobile-hidden="" class="wpb_row vc_row-fluid vc_row inner_row  vc_row-o-content-middle standard_section    " style="padding-top: 0px; padding-bottom: 2%; "><div class="row-bg-wrap"> <div class="row-bg   " style=""></div> </div><div class="col span_12  center">
	<div class="vc_col-sm-3 vc_hidden-sm vc_hidden-xs wpb_column column_container vc_column_container col child_column no-extra-padding instance-17" data-t-w-inherits="default" data-shadow="none" data-border-radius="none" data-border-animation="" data-border-animation-delay="" data-border-width="none" data-border-style="solid" data-border-color="" data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner"><div class="column-bg-overlay-wrap" data-bg-animation="none"><div class="column-bg-overlay"></div></div>
		<div class="wpb_wrapper">
			
		</div> 
	</div>
	</div> 

	<div style="margin-bottom: 2.5%!important; " class="vc_col-sm-6 wpb_column column_container vc_column_container col child_column no-extra-padding instance-18" data-t-w-inherits="default" data-shadow="none" data-border-radius="none" data-border-animation="" data-border-animation-delay="" data-border-width="none" data-border-style="solid" data-border-color="" data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner"><div class="column-bg-overlay-wrap" data-bg-animation="none"><div class="column-bg-overlay"></div></div>
		<div class="wpb_wrapper">
			
	<div class="wpb_raw_code wpb_content_element wpb_raw_html">
		<div class="wpb_wrapper">
			<script>(function() {
	window.mc4wp = window.mc4wp || {
		listeners: [],
		forms: {
			on: function(evt, cb) {
				window.mc4wp.listeners.push(
					{
						event   : evt,
						callback: cb
					}
				);
			}
		}
	}
})();
</script><!-- Mailchimp for WordPress v4.8.3 - https://wordpress.org/plugins/mailchimp-for-wp/ --><form id="mc4wp-form-1" class="mc4wp-form mc4wp-form-57 mc4wp-form-theme mc4wp-form-theme-light" method="post" data-id="57" data-name="pretty green"><div class="mc4wp-form-fields"><div class="row">
<div class="vc_col-sm-12">
  
 <input type="email" name="EMAIL" placeholder="Enter your email address" required="" style="
    width: 70%;
    display: inline;
    margin-right: 5%;
"><input type="submit" value="Sign up" style="
    width: 25%;
    display: inline;
    padding: 8px 16px !important;
">
      

</div>

</div></div><label style="display: none !important;">Leave this field empty if you're human: <input type="text" name="_mc4wp_honeypot" value="" tabindex="-1" autocomplete="off"></label><input type="hidden" name="_mc4wp_timestamp" value="1614731020"><input type="hidden" name="_mc4wp_form_id" value="57"><input type="hidden" name="_mc4wp_form_element_id" value="mc4wp-form-1"><div class="mc4wp-response"></div></form><!-- / Mailchimp for WordPress Plugin -->
		</div>
	</div>

		</div> 
	</div>
	</div> 

	<div class="vc_col-sm-3 wpb_column column_container vc_column_container col child_column no-extra-padding instance-19" data-t-w-inherits="default" data-shadow="none" data-border-radius="none" data-border-animation="" data-border-animation-delay="" data-border-width="none" data-border-style="solid" data-border-color="" data-bg-cover="" data-padding-pos="all" data-has-bg-color="false" data-bg-color="" data-bg-opacity="1" data-hover-bg="" data-hover-bg-opacity="1" data-animation="" data-delay="0">
		<div class="vc_column-inner"><div class="column-bg-overlay-wrap" data-bg-animation="none"><div class="column-bg-overlay"></div></div>
		<div class="wpb_wrapper">
			
		</div> 
	</div>
	</div> 
</div></div>
			</div> 
		</div>
	</div> 
</div></div>
<div id="footer-outer" <?php nectar_footer_attributes(); ?>>
	
	<?php
	
	get_template_part( 'includes/partials/footer/call-to-action' );
	
	get_template_part( 'includes/partials/footer/main-widgets' );
	
	get_template_part( 'includes/partials/footer/copyright-bar' );
	
	?>
	
</div><!--/footer-outer-->

<?php

nectar_hook_before_outer_wrap_close();

get_template_part( 'includes/partials/footer/off-canvas-navigation' );

?>

</div> <!--/ajax-content-wrap-->

<?php
	
	// Boxed theme option closing div.
	if ( ! empty( $nectar_options['boxed_layout'] ) && 
	'1' === $nectar_options['boxed_layout'] && 
	'left-header' !== $header_format ) {

		echo '</div><!--/boxed closing div-->'; 
	}
	
	get_template_part( 'includes/partials/footer/back-to-top' );
	get_template_part( 'includes/partials/footer/body-border' );
	
	nectar_hook_after_wp_footer();
	nectar_hook_before_body_close();
	
	wp_footer();
?>
</body>
</html>